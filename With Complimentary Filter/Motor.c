#define STEP 1
#define DIRE 0

void driveStepperMotor(int output)
{

	int steps = abs(output) * 4;
	int i = 0;

	if(output > 0)
	{
		digitalWrite(DIRE, 1);
	}
	else if(output < 0)
	{
		digitalWrite(DIRE, 0);
	}

	for(i = 0; i < steps*3; i ++)
	{
		digitalWrite(STEP, 1);
		delay(1);
		digitalWrite(STEP, 0);
		delay(1);
	}

}

void initStepperMotor()
{
	if(wiringPiSetup() == -1)
	{
		printf("Could not initialize WiringPi");
		exit(1);
	}
	pinMode(STEP, OUTPUT);
	pinMode(DIRE, OUTPUT);
}
