#define AA 0.98
#define AAinv 0.02

void calculateComplimentary(struct gyroAndAccNums * nums)
{
	nums->sensor_angle[0] = 0.98 * (nums->sensor_angle[0] 
		+ nums->gyro_gain[0] * 0.02) + 0.02 * nums->acc_angle[0];
	nums->sensor_angle[1] = 0.98 * (nums->sensor_angle[1] 
		+ nums->gyro_gain[1] * 0.02) + 0.02 * nums->acc_angle[1];
}
