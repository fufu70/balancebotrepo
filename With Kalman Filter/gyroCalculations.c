#define A_GAIN 0.0573;
#define G_GAIN 0.070;
#define DT 0.02;
#define RAD_TO_DEG 57.29578;

struct gyroAndAccNums
{
	int acc_raw[3];
	int gyro_raw[3];
	float acc_gain[3];
	float gyro_gain[3];
	float acc_angle[3];
	float gyro_angle[3];	
	float sensor_angle[3];
};

void initGyroAndAcc(struct gyroAndAccNums * nums)
{
	nums->gyro_gain[0] = 0.0;
	nums->gyro_gain[1] = 0.0;
	nums->gyro_gain[2] = 0.0;
	nums->sensor_angle[0] = 0.0;
	nums->sensor_angle[1] = 0.0;
	nums->sensor_angle[2] = 0.0;
}

void calculateInAccGain(struct gyroAndAccNums * nums)
{
	nums->acc_gain[0]  = (float) nums->acc_raw[0] * A_GAIN;
	nums->acc_gain[1] = (float) nums->acc_raw[1] * A_GAIN;
	nums->acc_gain[2] = (float) nums->acc_raw[2] * A_GAIN; 
}

void calculateInGyroGain(struct gyroAndAccNums * nums)
{
	nums->gyro_gain[0] = (float) nums->gyro_raw[0] * G_GAIN;
	nums->gyro_gain[1] = (float) nums->gyro_raw[1] * G_GAIN;
	nums->gyro_gain[2] = (float) nums->gyro_raw[2] * G_GAIN; 
}

void calculateGyroAngles(struct gyroAndAccNums * nums)
{
	nums->gyro_angle[0] += nums->gyro_gain[0] * DT;
	nums->gyro_angle[1] += nums->gyro_gain[1] * DT;
	nums->gyro_angle[2] += nums->gyro_gain[2] * DT;
}

void calculateAccAngles(struct gyroAndAccNums * nums)
{
	nums->acc_angle[0] = (float) (atan2(nums->acc_gain[1], nums->acc_gain[2])+M_PI)*RAD_TO_DEG;
	nums->acc_angle[1] = (float) (atan2(nums->acc_gain[2], nums->acc_gain[0])+M_PI)*RAD_TO_DEG;
}

void changeRotation(struct gyroAndAccNums * nums)
{
	if( nums->acc_angle[0] > 180)
	{
		nums->acc_angle[0] -= (float)360.0;
	}
	
	if( nums->acc_angle[1] > 180)
	{
		nums->acc_angle[1] -= (float)360.0;
	}
}
