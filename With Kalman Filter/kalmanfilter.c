/**
 * 	Thanks to Jose Luis Corona Miranda, for using his paper and his description,
 * 	and explanation of the Kalman Filter, for without it this project would not
 * 	be very effective.
 * 	
 *	The Kalman Filter file is used because of the potential of noisey occurences
 *	that will be coming in from the sensor.
 */


struct kalman
{
	double x_bias;
	double x_rate;
	double x_angle;

	double P_00;
	double P_01;
	double P_10;
	double P_11;

	double dt;
	double R_Angle;
	double Q_Gyro;
	double Q_Angle;
};

void kalmanInit(struct kalman * filter, double dt, double R_Angle, double Q_Gyro, double Q_Angle);
void kalmanPredict(struct kalman * filter, double gyro_rate);
void kalmanUpdate(struct kalman * filter, double angle_measured);


void kalmanInit(struct kalman * filter, double dt, double R_Angle, double Q_Gyro, double Q_Angle)
{
	filter->x_bias = 0;
	filter->x_rate = 0;
	filter->x_angle = 0;

	filter->dt = dt;
	filter->R_Angle = R_Angle;
	filter->Q_Gyro = Q_Gyro;
	filter->Q_Angle = Q_Angle;

	filter->P_00 = 1.0;
	filter->P_01 = 0;
	filter->P_10 = 0;
	filter->P_11 = 1.0;
}

void kalmanPredict(struct kalman * filter, double gyro_rate)
{
	static double gyro_rate_unbiased;
	static double Pdot_00;
	static double Pdot_01;
	static double Pdot_10;
	static double Pdot_11;

	gyro_rate_unbiased = gyro_rate - filter->x_bias;
	
	filter->x_bias = gyro_rate_unbiased;
	filter->x_angle = filter->x_angle + (gyro_rate -  filter->x_bias) * filter->dt;
	
	Pdot_00 = filter->Q_Angle - filter->P_01 - filter->P_10;
	Pdot_01 = -filter->P_11;
	Pdot_10 = -filter->P_11;
	Pdot_11 = filter->Q_Gyro;

	filter->P_00 += filter->P_00 * filter->dt; 
	filter->P_01 +=	filter->P_01 * filter->dt;
	filter->P_10 += filter->P_10 * filter->dt;
	filter->P_11 += filter->P_11 * filter->dt;
}

void kalmanUpdate(struct kalman * filter, double angle_measured)
{
	static double y;
	static double S;
	static double K_0;
	static double K_1;

	y = angle_measured - filter->x_angle;
	S = filter->R_Angle + filter->P_00;

	K_0 = filter->P_00 / S;
	K_1 = filter->P_10 / S;

	filter->P_00 -= K_0 * filter->P_00;
	filter->P_01 -= K_0 * filter->P_01;
	filter->P_10 -= K_1 * filter->P_00;
	filter->P_11 -= K_1 * filter->P_01;

	filter->x_angle = filter->x_angle + K_0 * y;
	filter->x_angle = filter->x_angle + K_1 * y;
}