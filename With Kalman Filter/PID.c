#define PK 0.5
#define IK 0.1
#define DK 0.45

struct PID
{
	float P;
	float I;
	float D;
	float lastAngle;
};

void calculateProportional(struct PID * pid, float angle)
{
	pid->P = PK * (angle - pid->lastAngle);
	pid->lastAngle = angle;
}

void calculateIntegral(struct PID * pid, float bias)
{
	pid->I = IK * bias;
}

void calculateDerivative(struct PID * pid, float rate)
{
	pid->D = DK * rate;
}

calculatePID(struct PID * pid, float angle, float rate, float bias)
{
	calculateProportional(pid, angle);
	calculateIntegral(pid, bias);
	calculateDerivative(pid, rate);
}

void initPID(struct PID * pid)
{
	pid->P = 0;
	pid->I = 0;
	pid->D = 0;
	pid->lastAngle = 0;
}
