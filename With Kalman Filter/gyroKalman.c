#include <linux/i2c-dev.h>
#include <stdlib.h>
#include <stdint.h>
#include <signal.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>
#include <wiringPi.h>
#include <gertboard.h>
#include <piFace.h>
#include "L3G.h"
#include "LSM303.h"
#include "i2cWrite.c"
#include "gyroCalculations.c"
#include "kalmanfilter.c"
#include "PID.c"
#include "Motor.c"
#define INIT_TIME 300

void INThandler(int sig)
{
	signal(sig, SIG_IGN);
	exit(0);
}

int myMillis()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (tv.tv_sec) * 1000 + (tv.tv_usec)/1000;
}

int timeval_subtract(struct timeval * result, struct timeval * t1, struct timeval * t2)
{
	long int diff = (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
	result->tv_sec = diff / 1000000;
	result->tv_usec = diff % 1000000;
	return (diff < 0);
}

int main(int argv, char * argc[])
{

	signal(SIGINT, INThandler);
	setup();
	
	struct kalman filter;
	kalmanInit(&filter, 0.02, 0.590, 0.002, 0.001);

	struct gyroAndAccNums nums;
	initGyroAndAcc(&nums);	

	struct PID pid;
	initPID(&pid);
	
	initStepperMotor();

	float constant = 0;
	float output = 0;

	int motorOutput = 0;
	
	int startInt = myMillis();
	struct timeval tvBegin, tvEnd, tvDiff;	

	for(i = 0; i < INIT_TIME; i ++)
	{
		startInt = myMillis();
		readAcc(nums.acc_raw);
		readGyro(nums.gyro_raw);
		kalmanPredict(&filter, nums.gyro_angle[1]);

		calculateInAccGain(&nums);
		calculateInGyroGain(&nums);
		calculateGyroAngles(&nums);
		calculateAccAngles(&nums);
		changeRotation(&nums);
		
		calculateComplimentary(&nums);
		calculatePID(&pid, filter.x_angle, filter.x_rate, filter.x_bias);
	    constant = pid.P + pid.I + pid.D;
		
		kalmanUpdate(&filter, nums.acc_angle[1]);
		printf("Constant: %lf\n", constant);
		while(myMillis() - startInt < 20)
		{
			usleep(100);
		}
		printf("Loop Time %d \t", myMillis() - startInt);
	}
	while(1)
	{
		startInt = myMillis();
		readAcc(nums.acc_raw);
		readGyro(nums.gyro_raw);
		kalmanPredict(&filter, nums.gyro_angle[1]);

		calculateInAccGain(&nums);
		calculateInGyroGain(&nums);
		calculateGyroAngles(&nums);
		calculateAccAngles(&nums);
		changeRotation(&nums);

		kalmanUpdate(&filter, nums.acc_angle[1]);
		calculatePID(&pid, filter.x_angle, filter.x_rate, filter.x_bias);

	    output = pid.P + pid.I + pid.D - constant;
	    motorOutput = (int) output;

		printf("Filter Raw: %lf\t Gyro Raw: %d\n",
				 kalmanGetAngle(&filter), nums.gyro_angle[1]);
		while(myMillis() - startInt < 20)
		{
			usleep(100);
			if(motorOutput < 0)
			{
				digitalWrite(DIRE, 1);
				delay(1);
				digitalWrite(STEP, 0);
				delay(1);
				digitalWrite(STEP, 1);
				delay(1);
				digitalWrite(STEP, 0);
				delay(1);
				motorOutput --;
			}
			else if(motorOutput > 0)
			{
				digitalWrite(DIRE, 0);
				delay(1);
				digitalWrite(STEP, 1);
				delay(1);
				digitalWrite(STEP, 0);
				delay(1);
				digitalWrite(STEP, 1);
				delay(1);
				motorOutput ++;
			}
		}
		printf("Loop Time %d \t", myMillis() - startInt);
	}
	
	return 0;
}
